package simpledb;

import java.util.ArrayList;

/**
 * Knows how to compute some aggregate over a set of StringFields.
 */
public class StringAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;
    private int gbfield;
    private Type gbfieldtype;
    private int afield;
    private Op what;
    private ArrayList<Tuple> tuples;
    /**
     * Aggregate constructor
     * @param gbfield the 0-based index of the group-by field in the tuple, or NO_GROUPING if there is no grouping
     * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null if there is no grouping
     * @param afield the 0-based index of the aggregate field in the tuple
     * @param what aggregation operator to use -- only supports COUNT
     * @throws IllegalArgumentException if what != COUNT
     */

    public StringAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
	    	this.gbfield = gbfield;
	    	this.gbfieldtype = gbfieldtype;
	    	this.afield = afield;
	    	if (!what.equals(Op.COUNT))
	    	{
	    		throw new IllegalArgumentException("Count is the only allowed operator for StringAggregator.");
	    	}
	    	this.what = what;
	    	tuples = new ArrayList<Tuple>();
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the constructor
     * @param tup the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // Count is the only sane one here, so count it is.
	    	if (gbfield == -1)
	    	{
	    		if (tuples.size() == 0)
	    		{
	    			TupleDesc td = Utility.getTupleDesc(1);
	    			Tuple t = new Tuple(td);
	    			t.setField(0, new IntField(1));
	    			tuples.add(t);
	    		}
	    		else 
	    		{
	    			Tuple t = tuples.get(0);
	    			IntField i = (IntField) t.getField(0);
	    			int j = i.getValue();
	    			j++;
	    			IntField i2 = new IntField(j);
	    			tuples.get(0).setField(0, i2);
			}
	    	}
	    	else {
	    		Field f = tup.getField(gbfield);
	    		if (tuples.size() == 0)
	    		{
		    		Type[] ta = {gbfieldtype, Type.INT_TYPE};
		    		TupleDesc td = new TupleDesc(ta);
		    		Tuple t = new Tuple(td);
		    		t.setField(0, f);
		    		t.setField(1, new IntField(1));
		    		tuples.add(t);
	    		}
	    		else
	    		{
	    			boolean actionPerformed = false;
	    			for (int i = 0; i < tuples.size(); i++)
	    			{
	    				Tuple t = tuples.get(i);
	    				Field fc = t.getField(0);
	    				if (f.compare(Predicate.Op.EQUALS, fc))
	    				{
	    					IntField iif = (IntField) t.getField(1);
	    					int j = iif.getValue();
	    					t.setField(1, new IntField(j+1));
	    					actionPerformed = true;
	    					break;
	    				}
	    				else if (f.compare(Predicate.Op.LESS_THAN, fc))
	    				{
	    					Type[] ta = {gbfieldtype, Type.INT_TYPE};
	    					TupleDesc td = new TupleDesc(ta);
	    					Tuple tt = new Tuple(td);
	    					tt.setField(0, f);
	    					tt.setField(1, new IntField(1));
	    					tuples.add(i, tt);
	    					actionPerformed = true;
	    					break;
	    				}
	    			}
	    			if (!actionPerformed)
	    			{
	    				Type[] ta = {gbfieldtype, Type.INT_TYPE};
	    				TupleDesc td = new TupleDesc(ta);
	    				Tuple t = new Tuple(td);
	    				t.setField(0, f);
	    				t.setField(1, new IntField(1));
	    				tuples.add(t);
	    			}
	    		}
	    	}
    }

    /**
     * Create a DbIterator over group aggregate results.
     *
     * @return a DbIterator whose tuples are the pair (groupVal,
     *   aggregateVal) if using group, or a single (aggregateVal) if no
     *   grouping. The aggregateVal is determined by the type of
     *   aggregate specified in the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
        AggregatorIterator iai = new AggregatorIterator(tuples);
        return iai;
    }

}
