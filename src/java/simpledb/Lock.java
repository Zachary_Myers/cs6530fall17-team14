package simpledb;

public class Lock {

	public TransactionId tid;
	public Permissions perm;
	
	public Lock(TransactionId tid, Permissions perm) {
		this.tid = tid;
		this.perm = perm;
	}
	
	public int getId() {
		return ((int) tid.getId() << 16) + (perm.hashCode() >>> 16);
	}
	
}
