package simpledb;

public class CatalogTriple {
	private DbFile file = null;
	private String name = null;
	private String pkey = null;
	
	public CatalogTriple(DbFile F, String N, String PK)
	{
		file = F;
		name = N;
		pkey = PK;
	}
	
	public DbFile getDbFile()
	{
		return file;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getKey()
	{
		return pkey;
	}
}
