package simpledb;

import java.io.Serializable;
import java.util.*;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc implements Serializable {

    /**
     * A help class to facilitate organizing the information of each field
     * */
    public static class TDItem implements Serializable {

        private static final long serialVersionUID = 1L;

        /**
         * The type of the field
         * */
        public final Type fieldType;
        
        /**
         * The name of the field
         * */
        public final String fieldName;

        public TDItem(Type t, String n) {
            this.fieldName = n;
            this.fieldType = t;
        }

        public String toString() {
            return fieldName + "(" + fieldType + ")";
        }
    }
    
    private ArrayList<TDItem> TDList;

    /**
     * @return
     *        An iterator which iterates over all the field TDItems
     *        that are included in this TupleDesc
     * */
    public Iterator<TDItem> iterator() {
        // some code goes here
        return TDList.iterator();
    }

    private static final long serialVersionUID = 1L;

    /**
     * Create a new TupleDesc with typeAr.length fields with fields of the
     * specified types, with associated named fields.
     * 
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     * @param fieldAr
     *            array specifying the names of the fields. Note that names may
     *            be null.
     */
    public TupleDesc(Type[] typeAr, String[] fieldAr) {
        // some code goes here
    		TDList = new ArrayList<TDItem>();
    		for(int i = 0; i < typeAr.length; i++) {
    			if(i < fieldAr.length) {
    				TDList.add(new TDItem(typeAr[i], fieldAr[i]));
    			} else {
    				TDList.add(new TDItem(typeAr[i], null));
    			}
    		}
    }

    /**
     * Constructor. Create a new tuple desc with typeAr.length fields with
     * fields of the specified types, with anonymous (unnamed) fields.
     * 
     * @param typeAr
     *            array specifying the number of and types of fields in this
     *            TupleDesc. It must contain at least one entry.
     */
    public TupleDesc(Type[] typeAr) {
        // some code goes here
    		TDList = new ArrayList<TDItem>();
		for(int i = 0; i < typeAr.length; i++) {
			TDList.add(new TDItem(typeAr[i], null));
		}
    }

    /**
     * @return the number of fields in this TupleDesc
     */
    public int numFields() {
        // some code goes here
        return TDList.size();
    }

    /**
     * Gets the (possibly null) field name of the ith field of this TupleDesc.
     * 
     * @param i
     *            index of the field name to return. It must be a valid index.
     * @return the name of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public String getFieldName(int i) throws NoSuchElementException {
        // some code goes here
    		if(i < 0 || i >= TDList.size()) {
			throw new NoSuchElementException();
		}
    		if(TDList.get(i).fieldName == null) {
    			return "null";
    		}
        return TDList.get(i).fieldName;
    }
    
    /**
     * Sets the (possibly null) field name of the ith field of this TupleDesc.
     * 
     * @param i
     *            index of the field name to return. It must be a valid index.
     *        s
     *            new name of the field.
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public void setFieldName(int i, String s) throws NoSuchElementException {
        // some code goes here
    		if(i < 0 || i >= TDList.size()) {
			throw new NoSuchElementException();
		} else {
			TDItem temp = TDList.get(i);
			TDList.set(i, new TDItem(temp.fieldType, s));
		}
    }

    /**
     * Gets the type of the ith field of this TupleDesc.
     * 
     * @param i
     *            The index of the field to get the type of. It must be a valid
     *            index.
     * @return the type of the ith field
     * @throws NoSuchElementException
     *             if i is not a valid field reference.
     */
    public Type getFieldType(int i) throws NoSuchElementException {
        // some code goes here
    		if(i < 0 || i >= TDList.size()) {
    			throw new NoSuchElementException(Integer.toString(i));
    		}
        return TDList.get(i).fieldType;
    }

    /**
     * Find the index of the field with a given name.
     * 
     * @param name
     *            name of the field.
     * @return the index of the field that is first to have the given name.
     * @throws NoSuchElementException
     *             if no field with a matching name is found.
     */
    public int fieldNameToIndex(String name) throws NoSuchElementException {
        // some code goes here
    		Iterator<TDItem> iter = iterator();
    		int i = 0;
    		while(iter.hasNext()) {
    			TDItem item = iter.next();
    			if(item.fieldName == null) {
    				
    			} else if(item.fieldName.equals(name)) {
    				return i;
    			}
    			i += 1;
    		}
        throw new NoSuchElementException();
    }

    /**
     * @return The size (in bytes) of tuples corresponding to this TupleDesc.
     *         Note that tuples from a given TupleDesc are of a fixed size.
     */
    public int getSize() {
        // some code goes here
    		int size = 0;
    		Iterator<TDItem> iter = TDList.iterator();
    		while(iter.hasNext()) {
    			TDItem item = iter.next();
    			if(item.fieldType == Type.INT_TYPE) {
    				size += Type.INT_TYPE.getLen();
    			} else if(item.fieldType == Type.STRING_TYPE) {
    				size += Type.STRING_TYPE.getLen();
    			}
    		}
        return size;
    }

    /**
     * Merge two TupleDescs into one, with td1.numFields + td2.numFields fields,
     * with the first td1.numFields coming from td1 and the remaining from td2.
     * 
     * @param td1
     *            The TupleDesc with the first fields of the new TupleDesc
     * @param td2
     *            The TupleDesc with the last fields of the TupleDesc
     * @return the new TupleDesc
     */
    public static TupleDesc merge(TupleDesc td1, TupleDesc td2) {
        // some code goes here
    		ArrayList<TDItem> TDListTemp = new ArrayList<TDItem>();
    		TDListTemp.addAll(td1.TDList);
    		TDListTemp.addAll(td2.TDList);
    		Type[] typeAr = new Type[TDListTemp.size()];
    		String[] nameAr = new String[TDListTemp.size()];
    		Iterator<TDItem> iter = TDListTemp.iterator();
    		int i = 0;
    		while(iter.hasNext()) {
    			TDItem item = iter.next();
    			typeAr[i] = item.fieldType;
    			nameAr[i] = item.fieldName;
    			i += 1;
    		}
        return new TupleDesc(typeAr, nameAr);
    }

    /**
     * Compares the specified object with this TupleDesc for equality. Two
     * TupleDescs are considered equal if they are the same size and if the n-th
     * type in this TupleDesc is equal to the n-th type in td.
     * 
     * @param o
     *            the Object to be compared for equality with this TupleDesc.
     * @return true if the object is equal to this TupleDesc.
     */
    public boolean equals(Object o) {
        // some code goes here
    		if(o == null) {
    			return false;
    		}
    		if(o.getClass().equals(this.getClass())) {
    			TupleDesc temp = TupleDesc.class.cast(o);
    			if(temp.getSize() == this.getSize()) {
    				Iterator<TDItem> thisIter = this.TDList.iterator();
    				Iterator<TDItem> tempIter = temp.TDList.iterator();
    				while(thisIter.hasNext()) {
    					if(!thisIter.next().fieldType.equals(tempIter.next().fieldType)) {
    						return false;
    					}
    				}
    				return true;
    			} else {
    				return false;
    			}
    		} else {
    			return false;
    		}
    }

    public int hashCode() {
        // If you want to use TupleDesc as keys for HashMap, implement this so
        // that equal objects have equals hashCode() results
        throw new UnsupportedOperationException("unimplemented");
    }

    /**
     * Returns a String describing this descriptor. It should be of the form
     * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
     * the exact format does not matter.
     * 
     * @return String describing this descriptor.
     */
    public String toString() {
        // some code goes here
    		String temp = "<";
    		Iterator<TDItem> iter = iterator();
    		while(iter.hasNext()) {
    			TDItem item = iter.next();
    			temp = temp + item.fieldType.toString() + "(" + item.fieldName + "), ";
    		}
    		temp = temp.substring(0, temp.length() - 2) + ">";
        return temp;
    }
}
