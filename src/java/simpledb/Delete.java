package simpledb;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator {

    private static final long serialVersionUID = 1L;
    private TransactionId t;
    private DbIterator child;
    private boolean hasDeleted;
    /**
     * Constructor specifying the transaction that this delete belongs to as
     * well as the child to read from.
     * 
     * @param t
     *            The transaction this delete runs in
     * @param child
     *            The child operator from which to read tuples for deletion
     */
    public Delete(TransactionId t, DbIterator child) {
        // some code goes here
	    	this.t = t;
	    	this.child = child;
	    	hasDeleted = false;
    }

    public TupleDesc getTupleDesc() {
        // some code goes here
        return Utility.getTupleDesc(1);
    }

    public void open() throws DbException, TransactionAbortedException {
        // some code goes here
	    	super.open();
	    	child.open();
    }

    public void close() {
        // some code goes here
	    	super.close();
	    	child.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        // some code goes here
    		child.rewind();
    }

    /**
     * Deletes tuples as they are read from the child operator. Deletes are
     * processed via the buffer pool (which can be accessed via the
     * Database.getBufferPool() method.
     * 
     * @return A 1-field tuple containing the number of deleted records.
     * @see Database#getBufferPool
     * @see BufferPool#deleteTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        // some code goes here
    		if (hasDeleted && !child.hasNext()) {
			return null;
		}
	    	int killed = 0;
	    	while (child.hasNext())
	    	{
	    		try {
					Database.getBufferPool().deleteTuple(t, child.next());
		    		killed++;
				} catch (NoSuchElementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	hasDeleted = true;
	    	TupleDesc td = Utility.getTupleDesc(1);
	    	Tuple tt = new Tuple(td);
	    	tt.setField(0, new IntField(killed));
	    	return tt;
    }

    @Override
    public DbIterator[] getChildren() {
        // some code goes here
    	// still a little confusing, because class is only configured for one child.
    	DbIterator[] dbi = {child};
        return dbi;
    }

    @Override
    public void setChildren(DbIterator[] children) {
        // some code goes here
    	// confusing for the same reason as getChildren.
    	child = children[0];
    }

}
