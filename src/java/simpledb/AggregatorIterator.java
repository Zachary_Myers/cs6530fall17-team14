package simpledb;

import java.util.ArrayList;
import java.util.NoSuchElementException;

public class AggregatorIterator implements DbIterator{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int count;
	private ArrayList<Tuple> tuples;
	boolean opened = false;
	
	public AggregatorIterator(ArrayList<Tuple> tuples) {
		// TODO Auto-generated constructor stub
		this.tuples = tuples;
	}

	@Override
	public void open() throws DbException, TransactionAbortedException {
		// TODO Auto-generated method stub
		count = 0;
		opened = true;
	}

	@Override
	public boolean hasNext() throws DbException, TransactionAbortedException {
		// TODO Auto-generated method stub
		if (!opened)
		{
			throw new DbException("Iterator is closed.");
		}
		if (count >= tuples.size())
			{
				return false;
			}
		else
			{
				return true;
			}
	}

	@Override
	public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
		// TODO Auto-generated method stub
		if (!opened)
		{
			throw new DbException("Iterator is closed.");
		}
		if (this.hasNext())
		{
			Tuple t = tuples.get(count);
			count++;
			return t;
		}
		else
		{
			throw new NoSuchElementException("Out of bounds.");
		}
	}

	@Override
	public void rewind() throws DbException, TransactionAbortedException {
		// TODO Auto-generated method stub
		if (!opened)
		{
			throw new DbException("Iterator is closed.");
		}
		count = 0;
	}

	@Override
	public TupleDesc getTupleDesc() {
		// TODO Auto-generated method stub
		try
		{
			return tuples.get(0).getTupleDesc();
		}
		catch (Exception e)
		{
			return null;
		}
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		opened = false;
	}

}
