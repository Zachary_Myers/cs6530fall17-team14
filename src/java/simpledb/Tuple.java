package simpledb;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

import simpledb.Predicate.Op;
import simpledb.TupleDesc.TDItem;

/**
 * Tuple maintains information about the contents of a tuple. Tuples have a
 * specified schema specified by a TupleDesc object and contain Field objects
 * with the data for each field.
 */
public class Tuple implements Serializable {

    private static final long serialVersionUID = 1L;
    private TupleDesc td;
    private RecordId rid;
    private ArrayList<Field> fields;
    
    /**
     * Create a new tuple with the specified schema (type).
     *
     * @param td
     *            the schema of this tuple. It must be a valid TupleDesc
     *            instance with at least one field.
     */
    public Tuple(TupleDesc td) {
        // some code goes here
    		this.td = td;
    		Iterator<TDItem> iter = td.iterator();
    		fields = new ArrayList<Field>();
    		while(iter.hasNext()) {
    			iter.next();
    			fields.add(null);
    		}
    }

    /**
     * @return The TupleDesc representing the schema of this tuple.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        return td;
    }

    /**
     * @return The RecordId representing the location of this tuple on disk. May
     *         be null.
     */
    public RecordId getRecordId() {
        // some code goes here
        return rid;
    }

    /**
     * Set the RecordId information for this tuple.
     *
     * @param rid
     *            the new RecordId for this tuple.
     */
    public void setRecordId(RecordId rid) {
        // some code goes here
    		this.rid = rid;
    		
    }

    /**
     * Change the value of the ith field of this tuple.
     *
     * @param i
     *            index of the field to change. It must be a valid index.
     * @param f
     *            new value for the field.
     */
    public void setField(int i, Field f) {
        // some code goes here
    		if(i < 0 || i >= td.getSize()) {
    			throw new NoSuchElementException();
    		} else {
    			fields.set(i, f);
    		}
    }

    /**
     * @return the value of the ith field, or null if it has not been set.
     *
     * @param i
     *            field index to return. Must be a valid index.
     */
    public Field getField(int i) {
        // some code goes here
    	if(i < 0 || i >= td.getSize()) {
			throw new NoSuchElementException();
		} else {
			return fields.get(i);
		}
    }

    /**
     * Returns the contents of this Tuple as a string. Note that to pass the
     * system tests, the format needs to be as follows:
     *
     * column1\tcolumn2\tcolumn3\t...\tcolumnN
     *
     * where \t is any whitespace (except a newline)
     */
    public String toString() {
        // some code goes here
        String s = "";
        Iterator<Field> iter = fields.iterator();
        while(iter.hasNext()) {
        		s = s + iter.next().toString() + "\t";
        }
        s = s.substring(0, s.length() - 2);
        return s;
    }

    /**
     * @return
     *        An iterator which iterates over all the fields of this tuple
     * */
    public Iterator<Field> fields()
    {
        // some code goes here
        return fields.iterator();
    }

    /**
     * reset the TupleDesc of this tuple
     * */
    public void resetTupleDesc(TupleDesc td)
    {
        // some code goes here
    		this.td = td;
    		Iterator<TDItem> iter = td.iterator();
    		fields = new ArrayList<Field>();
    		while(iter.hasNext()) {
    			fields.add(null);
    		}
    }
    
    public boolean equals(Tuple t) {
    		if (getTupleDesc().equals(t.getTupleDesc())) {
    			Iterator<Field> iter1 = fields();
    			Iterator<Field> iter2 = t.fields();
    			while (iter1.hasNext() && iter2.hasNext()) {
    				if (!iter1.next().compare(Op.EQUALS, iter2.next())) {
    					return false;
    				}
    			}
    			return true;
    		}
    		return false;
    }
}
