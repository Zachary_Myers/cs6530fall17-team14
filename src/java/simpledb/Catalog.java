package simpledb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentHashMap.KeySetView;

/**
 * The Catalog keeps track of all available tables in the database and their
 * associated schemas.
 * For now, this is a stub catalog that must be populated with tables by a
 * user program before it can be used -- eventually, this should be converted
 * to a catalog that reads a catalog table from disk.
 * 
 * @Threadsafe
 */
public class Catalog {

	private ConcurrentHashMap<Integer, CatalogTriple> internal = null;
    /**
     * Constructor.
     * Creates a new, empty catalog.
     */
    public Catalog() {
        // create underlying datastructure for use.
    		internal = new ConcurrentHashMap<Integer, CatalogTriple>();
    }

    /**
     * Add a new table to the catalog.
     * This table's contents are stored in the specified DbFile.
     * @param file the contents of the table to add;  file.getId() is the identfier of
     *    this file/tupledesc param for the calls getTupleDesc and getFile
     * @param name the name of the table -- may be an empty string.  May not be null.  If a name
     * conflict exists, use the last table to be added as the table for a given name.
     * @param pkeyField the name of the primary key field
     */
    public void addTable(DbFile file, String name, String pkeyField) {
        // Look to match name.
	    	int[] DAR = findByName(name); //DAR is short for Detection and Ranging (from RADAR).
	    	// Add item to database.
	    	if (DAR[0] == 0)
	    	{
	    		internal.put(file.getId(), new CatalogTriple(file,name,pkeyField));
	    	}
	    	else
	    	{
	    		internal.remove(DAR[1]);
	    		internal.put(file.getId(), new CatalogTriple(file,name,pkeyField));
	    	}
    }

    public void addTable(DbFile file, String name) {
        addTable(file, name, "");
    }

    /**
     * Add a new table to the catalog.
     * This table has tuples formatted using the specified TupleDesc and its
     * contents are stored in the specified DbFile.
     * @param file the contents of the table to add;  file.getId() is the identfier of
     *    this file/tupledesc param for the calls getTupleDesc and getFile
     */
    public void addTable(DbFile file) {
        addTable(file, (UUID.randomUUID()).toString());
    }

    /**
     * Return the id of the table with a specified name,
     * @throws NoSuchElementException if the table doesn't exist
     */
    public int getTableId(String name) throws NoSuchElementException {
        // Iterate, looking for NAME. Return.
	    	int[] DAR = findByName(name);
	    	if (DAR[0] == 0)
	    		throw new NoSuchElementException();
	    	else
	    		return DAR[1];
    }
    
    private int[] findByName(String name)
    {
	    	int[] answers = new int[2];
	    	// Dump internal as values
	    	Set<Map.Entry<Integer, CatalogTriple>> ct = internal.entrySet();
	    	// Iterate through values, searching for name each time.
	    	Iterator<Map.Entry<Integer, CatalogTriple>> ii = ct.iterator();
	    	while (ii.hasNext())
	    	{
	    		Map.Entry<Integer, CatalogTriple> now = ii.next();
	    		if (now.getValue().getName().equals(name))
	    		{
	    	    	// If name is found, create tuple from element and search HashMap for it.
	    			answers[0] = 1;
	    			answers[1] = now.getKey();
	    			return answers;
	    		}
	    	}
	    	// Return both whether or not you found it, 0; and what the key was, 1.
	    	answers[0] = 0;
	    	answers[1] = 0;
	    	return answers;
    }

    /**
     * Returns the tuple descriptor (schema) of the specified table
     * @param tableid The id of the table, as specified by the DbFile.getId()
     *     function passed to addTable
     * @throws NoSuchElementException if the table doesn't exist
     */
    public TupleDesc getTupleDesc(int tableid) throws NoSuchElementException {
        // Iterate, looking for ID. Return.
	    	CatalogTriple ct = internal.get(tableid);
	    	if (ct == null)
	    		throw new NoSuchElementException();
	    	TupleDesc td = ct.getDbFile().getTupleDesc();
        return td;
    }

    /**
     * Returns the DbFile that can be used to read the contents of the
     * specified table.
     * @param tableid The id of the table, as specified by the DbFile.getId()
     *     function passed to addTable
     */
    public DbFile getDatabaseFile(int tableid) throws NoSuchElementException {
        // Iterate, looking for ID. Return.
	    	if (internal.containsKey(tableid))
	    		return internal.get(tableid).getDbFile();
	    	else
	    		throw new NoSuchElementException();
    }

    public String getPrimaryKey(int tableid) {
        // Iterate, looking for ID. Return.
        return internal.get(tableid).getKey();
    }

    public Iterator<Integer> tableIdIterator() {
        // Got to return some iterator. Need to remember how to write them.
	    	Set<Integer> keys = internal.keySet();
	    	Iterator<Integer> ii = keys.iterator();
        return ii;
    }

    public String getTableName(int id) {
        // Iterate for datastructure, looking for ID.
    	
        return internal.get(id).getName();
    }
    
    /** Delete all tables from the catalog */
    public void clear() {
        // Wipe internal however necessary.
    		internal.clear();
    }
    
    /**
     * Reads the schema from a file and creates the appropriate tables in the database.
     * @param catalogFile
     */
    public void loadSchema(String catalogFile) {
        String line = "";
        String baseFolder=new File(new File(catalogFile).getAbsolutePath()).getParent();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(new File(catalogFile)));
            
            while ((line = br.readLine()) != null) {
                //assume line is of the format name (field type, field type, ...)
                String name = line.substring(0, line.indexOf("(")).trim();
                //System.out.println("TABLE NAME: " + name);
                String fields = line.substring(line.indexOf("(") + 1, line.indexOf(")")).trim();
                String[] els = fields.split(",");
                ArrayList<String> names = new ArrayList<String>();
                ArrayList<Type> types = new ArrayList<Type>();
                String primaryKey = "";
                for (String e : els) {
                    String[] els2 = e.trim().split(" ");
                    names.add(els2[0].trim());
                    if (els2[1].trim().toLowerCase().equals("int"))
                        types.add(Type.INT_TYPE);
                    else if (els2[1].trim().toLowerCase().equals("string"))
                        types.add(Type.STRING_TYPE);
                    else {
                        System.out.println("Unknown type " + els2[1]);
                        System.exit(0);
                    }
                    if (els2.length == 3) {
                        if (els2[2].trim().equals("pk"))
                            primaryKey = els2[0].trim();
                        else {
                            System.out.println("Unknown annotation " + els2[2]);
                            System.exit(0);
                        }
                    }
                }
                Type[] typeAr = types.toArray(new Type[0]);
                String[] namesAr = names.toArray(new String[0]);
                TupleDesc t = new TupleDesc(typeAr, namesAr);
                HeapFile tabHf = new HeapFile(new File(baseFolder+"/"+name + ".dat"), t);
                addTable(tabHf,name,primaryKey);
                System.out.println("Added table : " + name + " with schema " + t);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IndexOutOfBoundsException e) {
            System.out.println ("Invalid catalog entry : " + line);
            System.exit(0);
        }finally {
			// Close the file on success or error
			try {
				if (br != null)
					br.close();
			} catch (IOException ioe) {
				// Ignore failures closing the file
			}
		}
    }
}

