package simpledb;

import java.util.*;

/**
 * The Aggregation operator that computes an aggregate (e.g., sum, avg, max,
 * min). Note that we only support aggregates over a single column, grouped by a
 * single column.
 */
public class Aggregate extends Operator {

    private static final long serialVersionUID = 1L;
    private DbIterator child;
    private int afield;
    private int gfield;
    private Aggregator.Op aop;
    private Aggregator aggie;
    private DbIterator aggieIt;
    private boolean open = false;
    /**
     * Constructor.
     * 
     * Implementation hint: depending on the type of afield, you will want to
     * construct an {@link IntAggregator} or {@link StringAggregator} to help
     * you with your implementation of readNext().
     * 
     * 
     * @param child
     *            The DbIterator that is feeding us tuples.
     * @param afield
     *            The column over which we are computing an aggregate.
     * @param gfield
     *            The column over which we are grouping the result, or -1 if
     *            there is no grouping
     * @param aop
     *            The aggregation operator to use
     */
    public Aggregate(DbIterator child, int afield, int gfield, Aggregator.Op aop) {
	// some code goes here
    	this.child = child;
    	try {
			child.open();
		} catch (DbException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransactionAbortedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	this.afield = afield;
    	this.gfield = gfield;
    	this.aop = aop;
    }

    /**
     * @return If this aggregate is accompanied by a groupby, return the groupby
     *         field index in the <b>INPUT</b> tuples. If not, return
     *         {@link simpledb.Aggregator#NO_GROUPING}
     * */
    public int groupField() {
	// some code goes here
    	return gfield;
    }

    /**
     * @return If this aggregate is accompanied by a group by, return the name
     *         of the groupby field in the <b>OUTPUT</b> tuples If not, return
     *         null;
     * */
    public String groupFieldName() {
	// some code goes here
    	if (gfield != Aggregator.NO_GROUPING)
    	{
    		return child.getTupleDesc().getFieldName(gfield);
    	}
    	else
    	{
    		return null;
    	}
    }

    /**
     * @return the aggregate field
     * */
    public int aggregateField() {
	// some code goes here
    	return afield;
    }

    /**
     * @return return the name of the aggregate field in the <b>OUTPUT</b>
     *         tuples
     * */
    public String aggregateFieldName() {
	// some code goes here
    	return child.getTupleDesc().getFieldName(afield);
    }

    /**
     * @return return the aggregate operator
     * */
    public Aggregator.Op aggregateOp() {
	// some code goes here
    	return aop;
    }

    public static String nameOfAggregatorOp(Aggregator.Op aop) {
    	return aop.toString();
    }

    @Override
    public void open() throws NoSuchElementException, DbException,
	    TransactionAbortedException {
	    	// some code goes here
	    	//child.rewind();
	    	if (child.getTupleDesc().getFieldType(afield) == Type.STRING_TYPE)
	    	{
	    		if (gfield != Aggregator.NO_GROUPING)
	    		{
	    			aggie = new StringAggregator(gfield, child.getTupleDesc().getFieldType(gfield), afield, aop);
	    		}
	    		else
	    		{
	    			aggie = new StringAggregator(gfield, null, afield, aop);
	    		}
	    	}
	    	else if (child.getTupleDesc().getFieldType(afield) == Type.INT_TYPE)
	    	{
	    		if (gfield != Aggregator.NO_GROUPING)
	    		{
	    			aggie = new IntegerAggregator(gfield, child.getTupleDesc().getFieldType(gfield), afield, aop);
	    		}
	    		else
	    		{
	    			aggie = new IntegerAggregator(gfield, null, afield, aop);
	    		}
	    	}
	    	else
	    	{
	    		throw new DbException("Invalid Type for Aggregate.java");
	    	}
	    	while (child.hasNext())
	    	{
	    		aggie.mergeTupleIntoGroup(child.next());
	    	}
	    	aggieIt = aggie.iterator();
	    	aggieIt.open();
	    	this.open = true;
	    	super.open();
    }

    /**
     * Returns the next tuple. If there is a group by field, then the first
     * field is the field by which we are grouping, and the second field is the
     * result of computing the aggregate, If there is no group by field, then
     * the result tuple should contain one field representing the result of the
     * aggregate. Should return null if there are no more tuples.
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
    	// some code goes here
	    	if (!this.open)
	    	{
	    		throw new DbException("Not opened.");
	    	}
	    	if (aggieIt.hasNext())
	    	{
	    		return aggieIt.next();
	    	}
	    	else
	    	{	
	    		return null;
	    	}
    }
	
    public void rewind() throws DbException, TransactionAbortedException {
    	// some code goes here
	    	aggieIt.rewind();
    }

    /**
     * Returns the TupleDesc of this Aggregate. If there is no group by field,
     * this will have one field - the aggregate column. If there is a group by
     * field, the first field will be the group by field, and the second will be
     * the aggregate value column.
     * 
     * The name of an aggregate column should be informative. For example:
     * "aggName(aop) (child_td.getFieldName(afield))" where aop and afield are
     * given in the constructor, and child_td is the TupleDesc of the child
     * iterator.
     */
    public TupleDesc getTupleDesc() {
	// some code goes here
	    	if (gfield != Aggregator.NO_GROUPING)
	    	{
	    		Type[] ta = {child.getTupleDesc().getFieldType(gfield), Type.INT_TYPE};
	    		TupleDesc td = new TupleDesc(ta);
	    		td.setFieldName(0, child.getTupleDesc().getFieldName(gfield));
	    		td.setFieldName(1, aop.name() + " " + child.getTupleDesc().getFieldName(afield));
	    		return td;
	    	}
	    	else
	    	{
	    		Type[] ta = {Type.INT_TYPE};
	    		TupleDesc td = new TupleDesc(ta);
	    		td.setFieldName(0, aop.name() + " " + child.getTupleDesc().getFieldName(afield));
	    		return td;
	    	}
    }

    public void close() {
	// some code goes here
	    	this.open = false;
	    	super.close();
	    	aggieIt.close();
    }

    @Override
    public DbIterator[] getChildren() {
    		// some code goes here
	    	DbIterator[] dbi = {child};
	    	return dbi;
    }

    @Override
    public void setChildren(DbIterator[] children) {
	    	// some code goes here
	    	// This section is confusing because the class is set up to only receive one child.  
	    	// using only the first child for now.
	    	child = children[0];
    }
    
}
