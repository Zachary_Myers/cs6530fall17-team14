package simpledb;

import java.util.ArrayList;

/** A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram {

	int buckets;
	int min;
	int max;
	int[] bucketCounts;
	int[] bucketMins;
	ArrayList<Integer> values;
	
    /**
     * Create a new IntHistogram.
     * 
     * This IntHistogram should maintain a histogram of integer values that it receives.
     * It should split the histogram into "buckets" buckets.
     * 
     * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
     * 
     * Your implementation should use space and have execution time that are both
     * constant with respect to the number of values being histogrammed.  For example, you shouldn't 
     * simply store every value that you see in a sorted list.
     * 
     * @param buckets The number of buckets to split the input value into.
     * @param min The minimum integer value that will ever be passed to this class for histogramming
     * @param max The maximum integer value that will ever be passed to this class for histogramming
     */
    public IntHistogram(int buckets, int min, int max) {
    		// some code goes here
    		this.buckets = buckets;
    		this.min = min;
    		this.max = max;
    		bucketCounts = new int[buckets];
    		bucketMins = new int[buckets];
    		double currentMin = min;
    		double step = ((double) max - min) / ((double) buckets);
    		for (int i = 0; i < buckets; i++) {
    			bucketMins[i] = (int) currentMin;
    			currentMin += step;
    		}
    		values = new ArrayList<Integer>();
    }

    /**
     * Add a value to the set of values that you are keeping a histogram of.
     * @param v Value to add to the histogram
     */
    public void addValue(int v) {
    		// some code goes here
    		int index = findBucket(v, 0, buckets - 1);
    		values.add(v);
    		bucketCounts[index] += 1;
    }
    
    /**
     * Find the bucket v should be placed in by performing a binary search
     * @param v Value to search for in binary search
     * @param low Index of lowest bucket to perform binary search on
     * @param high Index of highest bucket to perform binary search on
     * @return Index of bucket that v should be placed in
     */
    private int findBucket(int v, int low, int high) {
    		if (v < min) {
    			return -1;
    		} else if (v > max) {
    			return -2;
    		}
    		int half = (int) Math.ceil(((double)(high + low))/2.0);
    		if (low == high) {
    			return low;
    		} else if (v < bucketMins[half]) {
    			return findBucket(v, low, half - 1);
    		} else {
    			return findBucket(v, half, high);
    		}
    }

    /**
     * Estimate the selectivity of a particular predicate and operand on this table.
     * 
     * For example, if "op" is "GREATER_THAN" and "v" is 5, 
     * return your estimate of the fraction of elements that are greater than 5.
     * 
     * @param op Operator
     * @param v Value
     * @return Predicted selectivity of this particular operator and value
     */
    public double estimateSelectivity(Predicate.Op op, int v) {
    		// some code goes here
    		int index = findBucket(v, 0, buckets - 1);
    		double w;
    		double b_right;
    		double h;
    		double b_left;
    		if (index == -1) {
    			h = 0;
    			w = 1;
    			b_right = v;
    			b_left = v;
    		} else if (index == -2) {
    			h = 0;
    			w  = 1;
    			index = buckets;
    			b_right = v;
    			b_left = v;
    		} else if (index == buckets - 1) {
    			w = Math.abs(max - bucketMins[index]);
    			b_right = max;
        		h = bucketCounts[index];
        		b_left = bucketMins[index];
    		} else {
    			w = Math.abs(bucketMins[index + 1] - bucketMins[index]);
    			b_right = bucketMins[index + 1];
        		h = bucketCounts[index];
        		b_left = bucketMins[index];
    		}
    		double s;
    		switch (op) {
    			case EQUALS:
    				return (double) (h/w)/((double) values.size());
    				
    			case GREATER_THAN:
    				s = h * (b_right - (double) v);
    				for (int i = index + 1; i < buckets; i++) {
    					s += bucketCounts[i];
    				}
    				s = s / ((double) values.size());
    				if (s > 1.0) {
    					s = 1.0;
    				} else if (s < 0.0) {
    					s = 0.0;
    				}
    				return s;
    				
    			case GREATER_THAN_OR_EQ:
    				s = h * (b_right - (double) v) + h/w;
    				for (int i = index + 1; i < buckets; i++) {
    					s += bucketCounts[i];
    				}
    				s = s / ((double) values.size());
    				if (s > 1.0) {
    					s = 1.0;
    				} else if (s < 0.0) {
    					s = 0.0;
    				}
    				return s;
    				
    			case LESS_THAN:
    				s = h * ((double) v - b_left);
    				for (int i = index - 1; i > -1; i--) {
    					s += bucketCounts[i];
    				}
    				s = s / ((double) values.size());
    				if (s > 1.0) {
    					s = 1.0;
    				} else if (s < 0.0) {
    					s = 0.0;
    				}
    				return s;
    				
    			case LESS_THAN_OR_EQ:
    				s = h * ((double) v - b_left) + h / w;
    				for (int i = index - 1; i > -1; i--) {
    					s += bucketCounts[i];
    				}
    				s = s / ((double) values.size());
    				if (s > 1.0) {
    					s = 1.0;
    				} else if (s < 0.0) {
    					s = 0.0;
    				}
    				return s;
    				
    			case NOT_EQUALS:
    				return (1.0 - (double) (h/w)/((double) values.size()));
    		}
        return -1.0;
    }
    
    /**
     * @return
     *     the average selectivity of this histogram.
     *     
     *     This is not an indispensable method to implement the basic
     *     join optimization. It may be needed if you want to
     *     implement a more efficient optimization
     * */
    public double avgSelectivity()
    {
        // some code goes here
        return 1.0;
    }
    
    /**
     * @return A string describing this histogram, for debugging purposes
     */
    public String toString() {
        // some code goes here
        return null;
    }
}
