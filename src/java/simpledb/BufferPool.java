package simpledb;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Random;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 * 
 * @Threadsafe, all fields are final
 */
public class BufferPool {
    /** Bytes per page, including header. */
    private static final int PAGE_SIZE = 4096;

    private static int pageSize = PAGE_SIZE;
    
    private static final int TIMEOUT = 50;
    private int failCount = 30;
    
    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;
    
    private static int numPages;
    public ConcurrentHashMap<PageId, Page> pages;
    public ConcurrentHashMap<PageId, ArrayList<Lock>> pageLocks;
    public ConcurrentHashMap<TransactionId, Integer> attempts;

    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        // some code goes here
    		BufferPool.numPages = numPages;
    		pages = new ConcurrentHashMap<PageId, Page>();
    		pageLocks = new ConcurrentHashMap<PageId, ArrayList<Lock>>();
    		attempts = new ConcurrentHashMap<TransactionId, Integer>();
    }
    
    public static int getPageSize() {
      return pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void setPageSize(int pageSize) {
    		BufferPool.pageSize = pageSize;
    }
    
    // THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
    public static void resetPageSize() {
    		BufferPool.pageSize = PAGE_SIZE;
    }

    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, an page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public Page getPage(TransactionId tid, PageId pid, Permissions perm)
        throws TransactionAbortedException, DbException {
        // some code goes here
		boolean canGiveLock = true;
		boolean addNewLock = true;
		if(perm == null) {
			throw new DbException("Requesting a page with null permissions!");
		}
		if(tid == null) {
			throw new DbException("Transaction Id is Null!!!");
		}
    		synchronized (pageLocks) {
    			if(pageLocks.containsKey(pid)) {
		    		ArrayList<Lock> locks = pageLocks.get(pid);
		    		synchronized (locks) {
			    		Iterator<Lock> iter = locks.iterator();
			    		if(locks.size() == 0) {
			    			// No current locks on the page, so we give the page the lock and add them
			    			//pageLocks.put(pid, new ArrayList<Lock>());
			    			//ArrayList<Lock> tempLocks = pageLocks.get(pid);
			    			locks.add(new Lock(tid, perm));
					} else {
			    			while(iter.hasNext()) {
			    				Lock lock = iter.next();
				    			if(lock.tid.getId() == tid.getId()) {
				    				if(lock.perm == perm) {
				    					// This exact lock already exists
				    					canGiveLock = true;
				    					addNewLock = false;
				    					break;
				    				} else if (perm == Permissions.READ_ONLY) {
				    					// The transaction already has either a shared or exclusive lock on this page
				    					canGiveLock = true;
				    					addNewLock = false;
				    					break;
				    				} else {
				    					// In this case, the requested permissions are Read_Write so we need to upgrade to exclusive lock.
				    					// This is allowed if the only current lock on the page is this transaction.
				    					if(locks.size() == 1) {
				    						iter.remove();
				    						canGiveLock = true;
				    						addNewLock = true;
				    						break;
				    					} else {
				    						canGiveLock = false;
				    						addNewLock = false;
				    						break;
				    						//throw new DbException("Cannot upgrade to exclusive lock, a shared lock is held by multiple transactions.");
				    					}
				    				}
				    			} else {
				    				if(lock.perm == Permissions.READ_ONLY && perm == Permissions.READ_ONLY) {
				    	    				// Don't change canGiveLock status, currently a shared lock is requested and held by another transaction
				    					
				    				} else {
				    					canGiveLock = false;
				    					addNewLock = false;
				    					break;
				    					//throw new DbException("Lock is currently blocked by transaction: " + Long.toString(tid.getId()));
				    				}
				    			}
			    			}
			    			if (canGiveLock && addNewLock) {
			    				locks.add(new Lock(tid, perm));
			    			}
					}
		    		}
    			} else {
    				canGiveLock = true;
    				addNewLock = false;
    				ArrayList<Lock> locks = new ArrayList<Lock>();
    				locks.add(new Lock(tid, perm));
    				pageLocks.put(pid, locks);
    			}
    		}
    		if (canGiveLock) {
    			synchronized (attempts) {
    				attempts.put(tid, 0);
    			}
    			synchronized (pages) {
    				if(pages.size() == 0) {
    					Page temp = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
    	    				pages.put(pid, temp);
    	    				return temp;
    				} else if(pages.containsKey(pid)) {
    					return pages.get(pid);
    				} else {
    					Page temp = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
    					if(pages.size() >= numPages) {
	    	    				while(pages.size() >= numPages) {
	    	    					evictPage();
	    	    				}
	    	    				pages.put(pid, temp);
	    	    				return temp;
	    	    			} else {
	    	    				pages.put(pid, temp);
	    		    			return temp;
	    	    			}
    				}
    			}
    		} else {
    			// The Lock was Blocked, so we wait a bit then try again
    			Random rand = new Random();
    			int timeout = TIMEOUT + rand.nextInt(100);
    			try {
    				Thread.sleep(timeout);
    			} catch (InterruptedException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    			synchronized(attempts) {
    				if (attempts.containsKey(tid)) {
		    			if (attempts.get(tid) < failCount)
		    			{
		    				attempts.put(tid, attempts.get(tid) + 1);
		    			}
		    			else
		    			{
		    				throw new TransactionAbortedException();
		    			}
    				} else {
    					attempts.put(tid, 1);
    				}
    			}
    			// We were blocked, so after waiting some time, we try again
    			// Note: There may be a much better way to implement the blocking
    			return getPage(tid, pid, perm);
    		}
    		/*if(pages.size() == 0) {
    			Page temp = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
    			pages.put(pid, temp);
    			ArrayList<Lock> tempLocks = new ArrayList<Lock>();
    			tempLocks.add(new Lock(tid, perm));
    			pageLocks.put(pid, tempLocks);
    			return temp;
    		} else if(pages.containsKey(pid)) {
    			ArrayList<Lock> locks = pageLocks.get(pid);
    			boolean canGiveLock = true;
			synchronized(locks) {
    				Iterator<Lock> iter = locks.iterator();
	    			if(locks.size() == 0) {
	        			// No current locks on the page, so we give the page the lock and add them
	        			//pageLocks.put(pid, new ArrayList<Lock>());
	        			//ArrayList<Lock> tempLocks = pageLocks.get(pid);
	        			locks.add(new Lock(tid, perm));
	        			return pages.get(pid);
	    			} else {
		    			while(iter.hasNext()) {
		    				Lock lock = iter.next();
			    			if(lock.tid.getId() == tid.getId()) {
			    				if(lock.perm == perm) {
			    					// This exact lock already exists
			    					return pages.get(pid);
			    				} else if (perm == Permissions.READ_ONLY) {
			    					// The transaction already has either a shared or exclusive lock on this page
			    					return pages.get(pid);
			    				} else {
			    					// In this case, the requested permissions are Read_Write so we need to upgrade to exclusive lock.
			    					// This is allowed if the only current lock on the page is this transaction.
			    					if(locks.size() == 1) {
			    						locks.clear();
			    						locks.add(new Lock(tid, perm));
			    						return pages.get(pid);
			    					} else {
			    						canGiveLock = false;
			    						//throw new DbException("Cannot upgrade to exclusive lock, a shared lock is held by multiple transactions.");
			    					}
			    				}
			    			} else {
			    				if(lock.perm == Permissions.READ_ONLY && perm == Permissions.READ_ONLY) {
			    	    				// Don't change canGiveLock status, currently a shared lock is requested and held by another transaction
			    				} else {
			    					canGiveLock = false;
			    					//throw new DbException("Lock is currently blocked by transaction: " + Long.toString(tid.getId()));
			    				}
			    			}
		    			}
		    			if (canGiveLock) {
		    				locks.add(new Lock(tid, perm));
			    			return pages.get(pid);
		    			}
    				}
    			}
			//throw new DbException("Cannot give lock for unknown reason");
			Random rand = new Random();
			int timeout = TIMEOUT + rand.nextInt(1001);
			try {
				Thread.sleep(timeout);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (failCount < 20)
			{
				failCount ++;
			}
			else
			{
				throw new TransactionAbortedException();
			}
			// We were blocked, so after waiting some time, we try again
			// Note: There may be a much better way to implement the blocking
			return getPage(tid, pid, perm);
    		} else {
    			Page temp = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
    			if(pages.size() >= numPages) {
    				while(pages.size() >= numPages) {
    					evictPage();
    				}
    				pages.put(pid, temp);
    				ArrayList<Lock> locks = new ArrayList<Lock>();
    				locks.add(new Lock(tid, perm));
    				pageLocks.put(pid, locks);
    				return temp;
    			} else {
    				pages.put(pid, temp);
    				ArrayList<Lock> locks = new ArrayList<Lock>();
    				locks.add(new Lock(tid, perm));
    				pageLocks.put(pid, locks);
	    			return temp;
    			}
    		}*/
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param pid the ID of the page to unlock
     */
    public synchronized void releasePage(TransactionId tid, PageId pid) {
        // some code goes here
        // not necessary for lab1|lab2
    		ArrayList<Lock> locks = pageLocks.get(pid);
    		Iterator<Lock> iter = locks.iterator();
    		boolean hasLock = false;
    		while (iter.hasNext()) {
    			Lock lock = iter.next();
    			if(lock.tid.getId() == tid.getId()) {
    				hasLock = true;
    				iter.remove();
    			}
    		}
    		if(hasLock == false) {
    			System.out.println("Requesting to release a lock which does not exist!");
    		}
    		/*pageLocks.remove(pid);
    		pageLocks.put(pid, new ArrayList<Lock>());*/
    }

    /**
     * Release all locks associated with a given transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     */
    public void transactionComplete(TransactionId tid) throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
    		// This is easy...
    		transactionComplete(tid, true);
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public synchronized boolean holdsLock(TransactionId tid, PageId p) {
        // some code goes here
        // not necessary for lab1|lab2
    		ArrayList<Lock> locks = pageLocks.get(p);
    		Iterator<Lock> iter = locks.iterator();
    		while (iter.hasNext()) {
    			if(iter.next().tid.getId() == tid.getId()) {
    				return true;
    			}
    		}
        return false;
    }

    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public void transactionComplete(TransactionId tid, boolean commit)
        throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
	    	// On commit, flush all dirty pages associated with tid.
	    	// On abort, restore all pages to on-disk state.
	    	// No matter what, release all Bufferpool state regarding transaction
	    	// Also release all locks the transaction held.
	    	if (commit)
	    	{
	    		// Committing transaction.
	    		flushPages(tid);
	    	}
	    	else
	    	{
	    		// ABORT!  ABORT!
	    		abortPages(tid);
	    	}
	    	// Clear all locks.
	    	removeLocks(tid);
	    	synchronized (attempts) {
	    		attempts.remove(tid);
	    	}
    }

    /**
     * Add a tuple to the specified table on behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to and any other 
     * pages that are updated (Lock acquisition is not needed for lab2). 
     * May block if the lock(s) cannot be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public void insertTuple(TransactionId tid, int tableId, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
    		ArrayList<Page> usedPages = Database.getCatalog().getDatabaseFile(tableId).insertTuple(tid, t);
    		for (Page page : usedPages) {
    			page.markDirty(true, tid);
			pages.put(page.getId(), page);
    		}
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from and any
     * other pages that are updated. May block if the lock(s) cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty by calling
     * their markDirty bit, and adds versions of any pages that have 
     * been dirtied to the cache (replacing any existing versions of those pages) so 
     * that future requests see up-to-date pages. 
     *
     * @param tid the transaction deleting the tuple.
     * @param t the tuple to delete
     */
    public void deleteTuple(TransactionId tid, Tuple t)
        throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
    		Database.getCatalog().getDatabaseFile(t.getRecordId().getPageId().getTableId()).deleteTuple(tid, t);
//	    	for (Page page : pages.values())
//	    	{
//	    		try 
//	    		{
//	    			Database.getCatalog().getDatabaseFile(page.getId().getTableId()).deleteTuple(tid, t);
//	    			page.markDirty(true, tid);
//	    		}
//	    		catch (NullPointerException e)
//	    		{
//	    			//System.out.println("Null Pointer!");
//	    			
//	    		}
//	    	}
    }

    /**
     * Flush all dirty pages to disk.
     * NB: Be careful using this routine -- it writes dirty data to disk so will
     *     break simpledb if running in NO STEAL mode.
     */
    public synchronized void flushAllPages() throws IOException {
        // some code goes here
        // not necessary for lab1
    		for(Page page : pages.values()) {
    			flushPage(page.getId());
    		}
    }

    /** Remove the specific page id from the buffer pool.
        Needed by the recovery manager to ensure that the
        buffer pool doesn't keep a rolled back page in its
        cache.
        
        Also used by B+ tree files to ensure that deleted pages
        are removed from the cache so they can be reused safely
    */
    public synchronized void discardPage(PageId pid) {
        // some code goes here
        // not necessary for lab1
    		pages.remove(pid);
    }

    /**
     * Flushes a certain page to disk
     * @param pid an ID indicating the page to flush
     * @throws DbException 
     */
    private synchronized  void flushPage(PageId pid) throws IOException {
        // some code goes here
        // not necessary for lab1
    		Page temp = pages.get(pid);
		temp.markDirty(false, null);
		Database.getCatalog().getDatabaseFile(pid.getTableId()).writePage(temp);
    }

    /** Write all pages of the specified transaction to disk.
     */
    public synchronized  void flushPages(TransactionId tid) throws IOException {
        // some code goes here
        // not necessary for lab1|lab2
	    	for(Page page: pages.values())
	    	{
	    		TransactionId dirtier = page.isDirty();
	    		if(dirtier == null || dirtier != tid)
	    		{
	    			continue;
	    		}
	    		else
	    		{
	    			// Page is dirtied by this transaction. Fluusshhhhhh.....
	    			flushPage(page.getId());
	    		}
	    	}
    }
    
    public synchronized void abortPage(PageId pid)
    {
	    pages.remove(pid);
    }
    
    public synchronized void abortPages(TransactionId tid) throws IOException{
    	//Helper to mass abort pages.
    	for (Page page: pages.values())
    	{
    		TransactionId dirtier = page.isDirty();
    		if(dirtier == null || dirtier != tid)
    		{
    			continue;
    		}
    		else
    		{
    			abortPage(page.getId());
    		}
    	}
    }
    
    public synchronized void removeLocks(TransactionId tid) throws IOException{
    	// Helper to remove all locks that a transaction holds.
	    	for (ArrayList<Lock> locks : pageLocks.values())
	    	{
	    		if (locks == null || locks.size() == 0)
	    		{
	    			continue;
	    		}
	    		Iterator<Lock> iter = locks.iterator();
	    		while(iter.hasNext())
	    		{
	    			Lock lock = iter.next();
	    			if (lock.tid == tid)
	    			{
	    				iter.remove();
	    			}
	    		}
	    	}
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     */
    private synchronized void evictPage() throws DbException {
        // some code goes here
        // not necessary for lab1
    		Enumeration<PageId> keys = pages.keys();
    		/*if(keys.hasMoreElements()) {
    			Integer key = keys.nextElement();
    			try {
    				flushPage(pages.get(key).getId());
    				pages.remove(key);
    			} catch (IOException e) {
    				throw new DbException("IO Exception with flushpage. " + e.getMessage());
    			}
    		} else {
    			throw new DbException("There are no pages currently in the bufferpool!");
    		}*/
    		while(keys.hasMoreElements()) {
    			PageId key = keys.nextElement();
    			if(!pageLocks.containsKey(key)) {
    				// This page has no lock, so we can evict it
	    			try {
	    				flushPage(key);
	    				pages.remove(key);
	    				return;
	    			} catch (IOException e) {
	    				throw new DbException("IO Exception with flushpage. " + e.getMessage());
	    			}
	    		}
    			else {
    				if(pages.get(key).isDirty() == null) {
    					// We can evict this page, even though it is locked, it isn't dirty
    					discardPage(key);
	    				pages.remove(key);
	    				return;
    				} else {
    					// Do nothing, the page is dirty and locked!
    				}
    			}
    		}
    		throw new DbException("There are no pages in the bufferpool that can be evicted due to locking!");
    		
    }

}
