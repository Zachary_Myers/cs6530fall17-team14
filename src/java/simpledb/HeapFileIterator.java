package simpledb;

import java.util.Iterator;

public class HeapFileIterator extends AbstractDbFileIterator{

	private Iterator<Tuple> iter;
	private TransactionId tid;
	private int currentPgNo;
	private HeapFile file;
	
	public HeapFileIterator(TransactionId tid, HeapFile file){
		this.tid = tid;
		this.file = file;
	}
	
	@Override
	public void open() throws DbException, TransactionAbortedException {
		// TODO Auto-generated method stub
		currentPgNo = 0;
		HeapPage temp = (HeapPage) Database.getBufferPool().getPage(tid, new HeapPageId(file.getId(), currentPgNo), Permissions.READ_ONLY);
		iter = temp.iterator();
	}

	@Override
	public void rewind() throws DbException, TransactionAbortedException {
		// TODO Auto-generated method stub
		close();
		open();
	}

	@Override
	protected Tuple readNext() throws DbException, TransactionAbortedException {
		// TODO Auto-generated method stub
		if(iter == null) {
			return null;
		}
		if(iter.hasNext()) {
			return iter.next();
		} else {
			currentPgNo++;
			while (currentPgNo < file.numPages()) {
				int test = file.numPages();
				HeapPage temp = (HeapPage) Database.getBufferPool().getPage(tid, new HeapPageId(file.getId(), currentPgNo), Permissions.READ_ONLY);
				iter = temp.iterator();
				if(iter.hasNext()) {
					return iter.next();
				} else {
					currentPgNo++;
					continue;
				}
			} 
			return null;
		}
	}
	
	@Override
	public void close() {
		super.close();
		iter = null;
	}

}
