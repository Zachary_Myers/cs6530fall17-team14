package simpledb;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Inserts tuples read from the child operator into the tableId specified in the
 * constructor
 */
public class Insert extends Operator {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     *
     * @param t
     *            The transaction running the insert.
     * @param child
     *            The child operator from which to read tuples to be inserted.
     * @param tableId
     *            The table in which to insert tuples.
     * @throws DbException
     *             if TupleDesc of child differs from table into which we are to
     *             insert.
     */
    private TransactionId t;
    private DbIterator child;
    private int tableId;
    private boolean hasInserted;
    
    public Insert(TransactionId t, DbIterator child, int tableId)
            throws DbException {
        // some code goes here
    		this.t = t;
    		this.child = child;
    		this.tableId = tableId;
    		hasInserted = false;
    }

    public TupleDesc getTupleDesc() {
        // some code goes here
        return Utility.getTupleDesc(1);
    }

    public void open() throws DbException, TransactionAbortedException {
        // some code goes here
    		super.open();
    		child.open();
    }

    public void close() {
        // some code goes here
    		super.close();
    		child.close();
    }

    public void rewind() throws DbException, TransactionAbortedException {
        // some code goes here
    		child.rewind();
    }

    /**
     * Inserts tuples read from child into the tableId specified by the
     * constructor. It returns a one field tuple containing the number of
     * inserted records. Inserts should be passed through BufferPool. An
     * instances of BufferPool is available via Database.getBufferPool(). Note
     * that insert DOES NOT need check to see if a particular tuple is a
     * duplicate before inserting it.
     *
     * @return A 1-field tuple containing the number of inserted records, or
     *         null if called more than once.
     * @see Database#getBufferPool
     * @see BufferPool#insertTuple
     */
    protected Tuple fetchNext() throws TransactionAbortedException, DbException {
        // some code goes here
    		if (hasInserted && !child.hasNext()) {
    			return null;
    		}
	    	int read = 0;
	    	while (child.hasNext())
	    	{
	    		try {
					Database.getBufferPool().insertTuple(t, tableId, child.next());
		    			read++;
				} catch (NoSuchElementException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    	}
	    	hasInserted = true;
	    	TupleDesc td = Utility.getTupleDesc(1);
	    	Tuple tt = new Tuple(td);
	    	tt.setField(0, new IntField(read));
	    	return tt;
    }

    @Override
    public DbIterator[] getChildren() {
        // some code goes here
    		// still a little confusing, because class is only configured for one child.
    		DbIterator[] dbi = {child};
        return dbi;
    }

    @Override
    public void setChildren(DbIterator[] children) {
        // some code goes here
    		// confusing for the same reason as getChildren.
    		child = children[0];
    }
}
