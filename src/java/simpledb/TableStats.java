package simpledb;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import simpledb.Predicate.Op;

/**
 * TableStats represents statistics (e.g., histograms) about base tables in a
 * query. 
 * 
 * This class is not needed in implementing lab1, lab2 and lab3.
 */
public class TableStats {

    private static final ConcurrentHashMap<String, TableStats> statsMap = new ConcurrentHashMap<String, TableStats>();

    static final int IOCOSTPERPAGE = 1000;

    public static TableStats getTableStats(String tablename) {
        return statsMap.get(tablename);
    }

    public static void setTableStats(String tablename, TableStats stats) {
        statsMap.put(tablename, stats);
    }
    
    public static void setStatsMap(HashMap<String,TableStats> s)
    {
        try {
            java.lang.reflect.Field statsMapF = TableStats.class.getDeclaredField("statsMap");
            statsMapF.setAccessible(true);
            statsMapF.set(null, s);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }

    public static Map<String, TableStats> getStatsMap() {
        return statsMap;
    }

    public static void computeStatistics() {
        Iterator<Integer> tableIt = Database.getCatalog().tableIdIterator();

        System.out.println("Computing table stats.");
        while (tableIt.hasNext()) {
            int tableid = tableIt.next();
            TableStats s = new TableStats(tableid, IOCOSTPERPAGE);
            setTableStats(Database.getCatalog().getTableName(tableid), s);
        }
        System.out.println("Done.");
    }

    /**
     * Number of bins for the histogram. Feel free to increase this value over
     * 100, though our tests assume that you have at least 100 bins in your
     * histograms.
     */
    static final int NUM_HIST_BINS = 100;

    /**
     * Create a new TableStats object, that keeps track of statistics on each
     * column of a table
     * 
     * @param tableid
     *            The table over which to compute statistics
     * @param ioCostPerPage
     *            The cost per page of IO. This doesn't differentiate between
     *            sequential-scan IO and disk seeks.
     */
    
    private int tableid;
    private int ioCostPerPage;
    private HeapFile file;
    private int numTuples;
    private TransactionId tid;
    private DbFileIterator iter;
    private TupleDesc td;
    
    private ConcurrentHashMap<Integer, IntHistogram> intHistograms;
    private ConcurrentHashMap<Integer, StringHistogram> stringHistograms;
    
    public TableStats(int tableid, int ioCostPerPage) {
        // For this function, you'll have to get the
        // DbFile for the table in question,
        // then scan through its tuples and calculate
        // the values that you need.
        // You should try to do this reasonably efficiently, but you don't
        // necessarily have to (for example) do everything
        // in a single scan of the table.
        // some code goes here
    		this.tableid = tableid;
    		this.ioCostPerPage = ioCostPerPage;
    		file = (HeapFile) Database.getCatalog().getDatabaseFile(tableid);
    		tid = new TransactionId();
    		iter = file.iterator(tid);
    		intHistograms = new ConcurrentHashMap<Integer, IntHistogram>();
    		stringHistograms = new ConcurrentHashMap<Integer, StringHistogram>();
    		td = file.getTupleDesc();
    		
    		// Find the min and max for each int field
    		ConcurrentHashMap<Integer, Field> mins = new ConcurrentHashMap<Integer, Field>();
    		ConcurrentHashMap<Integer, Field> maxs = new ConcurrentHashMap<Integer, Field>();
    		boolean start = false;
    		numTuples = 0;
    		Tuple currentTuple;
    		try {
			iter.open();
	    		while(iter.hasNext()) {
	    			currentTuple = iter.next();
	    			numTuples += 1;
	    			if (!start) {
	    				start = true;
		    	    		for (int i = 0; i < td.numFields(); i++) {
		    	    			if (td.getFieldType(i).equals(Type.STRING_TYPE)) {
		    	    				stringHistograms.put(i, new StringHistogram(NUM_HIST_BINS));
		    	    			} else {
		    	    				mins.put(i, currentTuple.getField(i));
		    	    				maxs.put(i, currentTuple.getField(i));
		    	    			}
		    	    		}
	    			} else {
	    				for (int i = 0; i < td.numFields(); i++) {
		    	    			if (td.getFieldType(i).equals(Type.INT_TYPE)) {
		    	    				if (mins.get(i).compare(Op.GREATER_THAN, currentTuple.getField(i))) {
		    	    					mins.replace(i, currentTuple.getField(i));
		    	    				}
		    	    				if (maxs.get(i).compare(Op.LESS_THAN, currentTuple.getField(i))) {
		    	    					maxs.replace(i, currentTuple.getField(i));
		    	    				}
		    	    			}
		    	    		}
	    			}
	    		}
		} catch (DbException | TransactionAbortedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    		
    		// Now that we have the mins and maxs, create each histogram
    		Enumeration<Integer> keys = mins.keys();
    		while (keys.hasMoreElements()) {
    			Integer key = keys.nextElement();
    			intHistograms.put(key, new IntHistogram(NUM_HIST_BINS, mins.get(key).hashCode(), maxs.get(key).hashCode()));
    		}
    		
    		// Now that the histograms have been initialized, we need to rescan the table and add the values into the histogram
    		try {
				iter.rewind();
				while (iter.hasNext()) {
					currentTuple = iter.next();
					for (int i = 0; i < td.numFields(); i++) {
						if (currentTuple.getField(i).getType().equals(Type.INT_TYPE)) {
							intHistograms.get(i).addValue(currentTuple.getField(i).hashCode());
						} else {
							stringHistograms.get(i).addValue(currentTuple.getField(i).toString());
						}
					}
				}
			} catch (DbException | TransactionAbortedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    /**
     * Estimates the cost of sequentially scanning the file, given that the cost
     * to read a page is costPerPageIO. You can assume that there are no seeks
     * and that no pages are in the buffer pool.
     * 
     * Also, assume that your hard drive can only read entire pages at once, so
     * if the last page of the table only has one tuple on it, it's just as
     * expensive to read as a full page. (Most real hard drives can't
     * efficiently address regions smaller than a page at a time.)
     * 
     * @return The estimated cost of scanning the table.
     */
    public double estimateScanCost() {
        // some code goes herex
        return file.numPages() * ioCostPerPage;
    }

    /**
     * This method returns the number of tuples in the relation, given that a
     * predicate with selectivity selectivityFactor is applied.
     * 
     * @param selectivityFactor
     *            The selectivity of any predicates over the table
     * @return The estimated cardinality of the scan with the specified
     *         selectivityFactor
     */
    public int estimateTableCardinality(double selectivityFactor) {
        // some code goes here
        return (int) (selectivityFactor * ((double) numTuples));
    }

    /**
     * The average selectivity of the field under op.
     * @param field
     *        the index of the field
     * @param op
     *        the operator in the predicate
     * The semantic of the method is that, given the table, and then given a
     * tuple, of which we do not know the value of the field, return the
     * expected selectivity. You may estimate this value from the histograms.
     * */
    public double avgSelectivity(int field, Predicate.Op op) {
        // some code goes here
    		if (td.getFieldType(field).equals(Type.INT_TYPE)) {
    			return intHistograms.get(field).avgSelectivity();
    		} else {
    			return stringHistograms.get(field).avgSelectivity();
    		}
    }

    /**
     * Estimate the selectivity of predicate <tt>field op constant</tt> on the
     * table.
     * 
     * @param field
     *            The field over which the predicate ranges
     * @param op
     *            The logical operation in the predicate
     * @param constant
     *            The value against which the field is compared
     * @return The estimated selectivity (fraction of tuples that satisfy) the
     *         predicate
     */
    public double estimateSelectivity(int field, Predicate.Op op, Field constant) {
        // some code goes here
    	if (td.getFieldType(field).equals(Type.INT_TYPE)) {
			return intHistograms.get(field).estimateSelectivity(op, constant.hashCode());
		} else {
			return stringHistograms.get(field).estimateSelectivity(op, constant.toString());
		}
    }

    /**
     * return the total number of tuples in this table
     * */
    public int totalTuples() {
        // some code goes here
        return numTuples;
    }

}
