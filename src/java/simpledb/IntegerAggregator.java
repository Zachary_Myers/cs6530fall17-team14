package simpledb;

import java.util.ArrayList;
import java.util.HashMap;

import simpledb.Aggregator.Op;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;
    private int gbfield;
    private Type gbfieldtype;
    private int afield;
    private Op what;
    private ArrayList<Tuple> tuples;
    private int avgcount = 0;
    private int avgsum = 0;
    // averages actually is just a bunch of {avgsum, avgcount} arrays mapped to fields.
    private HashMap<Field, int[]> averages;
    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */

    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
    	this.gbfield = gbfield;
    	this.gbfieldtype = gbfieldtype;
    	this.afield = afield;
    	this.what = what;
    	tuples = new ArrayList<Tuple>();
    	averages = new HashMap<Field, int[]>();
    	if (gbfield == -1)
    	{
    		TupleDesc td = Utility.getTupleDesc(1);
    		Tuple t = new Tuple(td);
    		if (what == Aggregator.Op.MIN)
    		{
    			t.setField(0, new IntField(Integer.MAX_VALUE));
    		}
    		else if (what == Aggregator.Op.MAX)
    		{
    			t.setField(0, new IntField(Integer.MIN_VALUE));
    		}
    		else
    		{
    			t.setField(0, new IntField(0));
    		}
    		tuples.add(t);
    	}
    	
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
    	if (gbfield == -1)
    	{
    		// Number in merging Tuple
    		int a = ((IntField) tup.getField(afield)).getValue();
    		// Number already existing
    		int j = ((IntField) tuples.get(0).getField(0)).getValue();
    		switch (what)
    		{
    		case AVG:
    			// Compute average, which can be very tricky, considering old data is discarded.
    			// No suitable algorithm could be found that can do this without two helper variables.
    			avgsum += a;
    			avgcount++;
    			tuples.get(0).setField(0, new IntField(avgsum / avgcount));
    			break;
    		case COUNT:
    			// Just up the number, nothing special.
    			tuples.get(0).setField(0, new IntField(j+1));
    			break;
    		case MAX:
    			// If bigger number is found, set that.
    			if (a > j)
    			{
    				tuples.get(0).setField(0, new IntField(a));
    			}
    			break;
    		case MIN:
    			// If smaller number is found, set that.
    			if (a < j)
    			{
    				tuples.get(0).setField(0, new IntField(a));
    			}
    			break;
    		case SUM:
    			// Add a to j.
    			tuples.get(0).setField(0, new IntField(a + j));
    			break;
    		case SC_AVG:
    			// lab 7
    			break;
    		case SUM_COUNT:
    			// lab 7
    			break;
    		default:
    			break;
    		}
    	}
    	else
    	{
    		// Just got to look through the fields until I find the one that matches.
    		Field f = tup.getField(gbfield);
    		// Number in merging Tuple
    		int a = ((IntField) tup.getField(afield)).getValue();
    		boolean actionPerformed = false;
    		int insertLocation = -1;
    		for (int i = 0; i < tuples.size(); i++)
    		{
    			Field fc = tuples.get(i).getField(0);
    			if (f.compare(Predicate.Op.EQUALS, fc))
    			{
    				int j = ((IntField) tuples.get(i).getField(1)).getValue();
    				// Existing Tuple needs to be updated.
    				switch (what)
    		    	{
    				case AVG:
    					// Currently, the hardest. Need the help of the averages hashmap.
    					int[] info = averages.get(f);
    					int currentSum = info[0];
    					int currentCount = info[1];
    					currentSum += a;
    					currentCount++;
    					tuples.get(i).setField(1, new IntField(currentSum / currentCount));
    					info[0] = currentSum;
    					info[1] = currentCount;
    					averages.put(f, info);
    					break;
    				case COUNT:
    					// Easiest. Just increment value.
    					tuples.get(i).setField(1, new IntField(j+1));
    					break;
    				case MAX:
    					// Only if a (new value) is bigger than j (existing value)
    					if (a > j)
    					{
    						tuples.get(i).setField(1, new IntField(a));
    					}
    					break;
    				case MIN:
    					// Only if a (new value) is smaller than j (existing value)
    					if (a < j)
    					{
    						tuples.get(i).setField(1, new IntField(a));
    					}
    					break;
    				case SUM:
    					// Easy enough, just add a to value.
    					tuples.get(i).setField(1, new IntField(j+a));
    					break;
    				case SC_AVG:
    					// Waiting for Lab7.
    					break;
    				case SUM_COUNT:
    					// Waiting for Lab7.
    					break;
    				default:
    					// Should not be reachable.
    					break;
    		    	}
    				actionPerformed = true;
    				break;
    			}
    			else if (f.compare(Predicate.Op.LESS_THAN, fc))
    			{
    				// New Tuple needs to be inserted at this spot.
    				// To avoid copy-pasting code, the code inside !actionPerformed
    				// will actually create and insert the tuple, insertLocation is used
    				// to track where it should go.
    				actionPerformed = false;
    				insertLocation = i;
    				break;
    			}
    		}
    		if (!actionPerformed)
    		{
    			//New Tuple needs to be inserted.
    			// Create blank Tuple.
    			Type[] ta = {gbfieldtype, Type.INT_TYPE};
    			TupleDesc td = new TupleDesc(ta);
    			Tuple t = new Tuple(td);
    			// Set Group data.
    			t.setField(0, f);
    			// Set initial information. This is different depending on what the OP is.
    			switch (what)
    	    	{
    			case AVG:
    				// Average actually can safely initialize to whatever is in the new tuple,
    				// but also requires just a little bit more.
    				t.setField(1, new IntField(a));
    				// Prep seed for averages.
    				int[] info = {a, 1};
    				averages.put(f, info);
    				break;
    			case COUNT:
    				// Count just initializes a new group at 1.
    				t.setField(1, new IntField(1));
    			case MAX:
    			case MIN:
    			case SUM:
    				// Max, Min, and Sum all just initialize to whatever is in the new tuple.
    				t.setField(1, new IntField(a));
    				break;
    			case SC_AVG:
    				// Waiting for Lab7.
    				break;
    			case SUM_COUNT:
    				// Waiting for Lab7.
    				break;
    			default:
    				// Should not be reachable.
    				break;
    	    	}
    			if (insertLocation == -1)
    			{
    				tuples.add(t);
    			}
    			else
    			{
    				tuples.add(insertLocation, t);
    			}
    		}
    	}
    }

    /**
     * Create a DbIterator over group aggregate results.
     * 
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
        return new AggregatorIterator(tuples);
    }

}
