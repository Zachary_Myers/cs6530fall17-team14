package simpledb;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
	
	private File f;
	private TupleDesc td;
	private int numPages;
	
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
		this.f = f;
		this.td = td;
		
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        return f;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        return f.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        return td;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
    		int BytesToRead = BufferPool.getPageSize();
    		byte[] reading = new byte[BytesToRead];
    		RandomAccessFile raf = null;
    		try {
				raf = new RandomAccessFile(f, "r");
				int readLength;
				if(raf.length() - (pid.pageNumber())*BytesToRead < BytesToRead) {
					readLength = (int) raf.length() - (pid.pageNumber())*BytesToRead;
				} else {
					readLength = BytesToRead;
				}
				long offset = (pid.pageNumber())*BytesToRead;
				raf.seek(offset);
				raf.read(reading, 0, BytesToRead);
		        raf.close();
		        return new HeapPage((HeapPageId) pid, reading);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// Close the file on success or error
				try {
					if (raf != null)
						raf.close();
				} catch (IOException ioe) {
					// Ignore failures closing the file
				}
			}
    		
    		return null;
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
    	int BytesToRead = BufferPool.getPageSize();
    	byte[] toWrite = page.getPageData();
    	RandomAccessFile raf = null;
    	try
    	{
    		raf = new RandomAccessFile(f, "rw");
    		long offset = (page.getId().pageNumber())*BytesToRead;
    		raf.seek(offset);
    		raf.write(toWrite);
    		raf.close();
    	} catch (Exception e) {
    		e.printStackTrace();
    	} finally {
    		try {
    			if (raf != null)
    				raf.close();
    		} catch (IOException ioe) {
    			
    		}
    	}
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
    		if (numPages == 0) {
	    		int ps = BufferPool.getPageSize();
	    		RandomAccessFile raf = null;
	    		try {
				raf = new RandomAccessFile(f, "r");
				long len = raf.length();
				numPages = (int) Math.ceil(len/ps);
				return numPages;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// Close the file on success or error
				try {
					if (raf != null)
						raf.close();
				} catch (IOException ioe) {
					// Ignore failures closing the file
				}
			}
	        return 0;
    		} else {
    			return numPages;
    		}
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
    		
    		// Find empty page
    		int currentPgNo = 0;
    		HeapPageId pid = null;
    		HeapPage temp = null;
    		while (currentPgNo < numPages()) {
			pid = new HeapPageId(getId(), currentPgNo);
    			temp = (HeapPage) Database.getBufferPool().getPage(tid, pid, Permissions.READ_ONLY);
    			if (temp.getNumEmptySlots() > 0) {
    				temp = (HeapPage) Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
    				temp.insertTuple(t);
    				temp.markDirty(true, tid);
    				ArrayList<Page> pages = new ArrayList<Page>();
    				pages.add(temp);
    		        return pages;
    			} else {
    				currentPgNo++;
    			}
    		}
    		// Make a new page if we go over the page limit
    		pid = new HeapPageId(getId(), currentPgNo);
    		temp = (HeapPage) Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
    		temp.insertTuple(t);
    		temp.markDirty(true, tid);
    		ArrayList<Page> pages = new ArrayList<Page>();
		pages.add(temp);
		numPages += 1;
        return pages;
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        // not necessary for lab1
    		if (t.getRecordId() == null) {
    			throw new DbException("The passed tuple does not have a RecordId!");
    		}
		HeapPage temp = (HeapPage) Database.getBufferPool().getPage(tid, t.getRecordId().getPageId(), Permissions.READ_WRITE);
		temp.deleteTuple(t);
		ArrayList<Page> pages = new ArrayList<Page>();
		pages.add(temp);
		return pages;
    }

    // see DbFile.java for javadocs
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
    			return new HeapFileIterator(tid, this);
    }

}

